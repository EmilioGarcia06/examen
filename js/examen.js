const btnBuscar = document.getElementById('btnBuscar');
const btnLimpiar = document.getElementById('btnLimpiar');
const tabla = document.getElementById('tabla');
const tbody = document.getElementById('tbody');
const mensaje = document.getElementById('mensaje');
const usuarios = document.getElementById('usuarios');
const name = document.getElementById('name');

btnBuscar.addEventListener('click', cargarDatos);
btnLimpiar.addEventListener('click', limpiar);
document.addEventListener('DOMContentLoaded', nombres);

function nombres() {
    const url = "https://jsonplaceholder.typicode.com/users";

    fetch(url)
        .then(response => response.json())// toma la respuesta y la convierte en json 
        .then(data => {
            nombres.innerHTML= name;
            data.forEach(usuario => {
                const option = document.createElement('option');
                option.value = usuario.id; //guarda el id 
                option.textContent = usuario.name;
                usuarios.appendChild(option);
            });
        })
        .catch(error => {
            console.error("Error al cargar nombres", error);
        });
}

function cargarDatos() {
    const id = usuarios.value;
    if (!id) {
        mensaje.innerHTML = "Seleccione un usuario por favor.";
        return;
    }

    const url = `https://jsonplaceholder.typicode.com/users/${id}`;

    axios.get(url)
        .then(response => {
            mostrarUsuario(response.data);
        })
        .catch(error => {
            mensaje.innerHTML = "Surgió un error: " + error;
        });
}

function mostrarUsuario(usuario) {

    const fila = document.createElement('tr');

    const col1 = document.createElement('td');
    col1.textContent = usuario.id;
    fila.appendChild(col1);

    const col2 = document.createElement('td');
    col2.textContent = usuario.name;
    fila.appendChild(col2);

    const col3 = document.createElement('td');
    col3.textContent = usuario.username;
    fila.appendChild(col3);

    const col4 = document.createElement('td');
    col4.textContent = usuario.email;
    fila.appendChild(col4);

    const col5 = document.createElement('td');
    col5.textContent = usuario.phone;
    fila.appendChild(col5);

    const col6 = document.createElement('td');
    col6.textContent = usuario.address.city;
    fila.appendChild(col6);

    tbody.appendChild(fila);
    mensaje.innerHTML = "Cantidad de registros: 1";
}

function limpiar() {
    tbody.innerHTML = "";
    mensaje.innerHTML = "Cantidad de registros: 0";
    document.getElementById('usuarios').value="";
    
}
